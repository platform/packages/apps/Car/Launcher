//
// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_library {
    name: "CarDockUtilLib",
    srcs: [
        "src/**/*.java",
        "src/**/*.kt",
        ":hidden_api_enabled_srcs",
    ],

    resource_dirs: ["res"],

    static_libs: [
        "dock_flags_java_lib",
        "androidx.lifecycle_lifecycle-extensions",
    ],

    manifest: "AndroidManifest.xml",
}

aconfig_declarations {
    name: "dock_flags",
    package: "com.android.car.dockutil",
    container: "system",
    srcs: ["dock_flags.aconfig"],
}

java_aconfig_library {
    name: "dock_flags_java_lib",
    aconfig_declarations: "dock_flags",
}
